# coding=utf8
import urllib
from pyquery import PyQuery
from video.models import Video
from collector import Collector

import logging

logger = logging.getLogger("collector")

#youku collector 
class YoukuCollector(Collector):
    def getVideos(self, content):
        pq = PyQuery(content)
        v_items = pq.find("#vcoll .items .v")
        videos = []
        def callback(index, item):
            item = PyQuery(item)
            video = Video()
            py_title = item.find(".v_title a").eq(0)
            play_len = item.find(".v_time .num").text()
            ref_url = py_title.attr("href")
            try:
                checkV = Video.objects.get(ref_url = ref_url)
                logger.info("ref_url : " + ref_url + " has existed, pass it") 
                return
            except Exception:
                pass
            title = py_title.attr("title")
            p_img_url =item.find(".v_thumb img").attr("src")
            hits = item.find(".v_stat .num").text().replace(",","")
            video.title = title
            video.play_length = play_len
            video.ref_url = ref_url
            video.preview_img_url = p_img_url
            video.hits = hits
            videos.append(video)
        v_items.each(callback)
        return videos
    
    def completeVideo(self, video):
        content = urllib.urlopen(video.ref_url).read()
        pg = PyQuery(content)
        swf_url = pg.find("#link2").val()
        video.swf_url = swf_url

