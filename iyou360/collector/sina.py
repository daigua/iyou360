# coding=utf8
import urllib
from pyquery import PyQuery
from video.models import Video,Category
from collector import Collector
from models import CollectSource
import random
import logging

logger = logging.getLogger("collector")
#sina collector 
class Diablo3Collector(Collector):
    def getVideos(self, content):
        pq = PyQuery(content)
        v_items = pq.find(".ListList li")
        videos = []
        def callback(index, item):
            item = PyQuery(item)
            ref_url = item.find(".div_img3_z").attr("href")
            try:
                Video.objects.get(ref_url = ref_url)
                logger.info("ref_url : " + ref_url + " has existed, pass it") 
                return
            except Exception:
                pass
            video = Video()
            thumb = item.find(".div_img3_z img")
            title = thumb.attr("alt")
            p_img_url =item.find(".div_img3_z img").attr("src")
            hits = random.randint(50, 239)
            category_name = item.find(".li_font12").eq(0).html()[3:]
            category_name=category_name.lstrip()
            category_name=category_name.rstrip()
            category_name=category_name.strip()
            if(category_name and len(category_name) > 0):
                if(category_name == u"法师"):
                    category_name = u"秘术师"
                try:
                    category = Category.objects.get(name=category_name)
                    video.lazy_categorys = [category]
                except Video.DoesNotExist:
                    logger.info("分类不存在：" + category_name)
            video.title = title
            video.ref_url = ref_url
            video.preview_img_url = p_img_url
            video.hits = hits
            videos.append(video)
        v_items.each(callback)
        return videos
    
    def completeVideo(self, video):
        content = urllib.urlopen(video.ref_url).read()
        pg = PyQuery(content)
        swf_url = pg.find(".Center_main_video embed").attr("src")
        summary = pg.find(".Center_font_con").html()
        video.summary = summary
        video.swf_url = swf_url

class WowCollector(Collector):
    def getVideos(self, content):
        pq = PyQuery(content)
        v_items = pq.find(".sp_list .z_l_list")
        videos = []
        def callback(index, item):
            item = PyQuery(item)
            video = Video()
            py_title = item.find(".pic a")
            play_len = ""
            ref_url = py_title.attr("href")
            try:
                checkV = Video.objects.get(ref_url = ref_url)
                logger.info("ref_url : " + ref_url + " has existed, pass it") 
                return
            except Exception:
                pass
            title = py_title.attr("title")
            p_img_url =py_title.find("img").attr("src")
            hits = random.randint(50, 239)
            video.title = title
            video.play_length = play_len
            video.ref_url = ref_url
            video.preview_img_url = p_img_url
            video.hits = hits
            videos.append(video)
        v_items.each(callback)
        return videos
    
    def completeVideo(self, video):
        content = urllib.urlopen(video.ref_url).read()
        pg = PyQuery(content)
        swf_url = pg.find("#ssss embed").attr("src")
        summary = pg.find(".flvxx").html()
        video.summary = summary[summary.find("<br />")+6:]
        video.swf_url = swf_url

if(__name__=="__main__"):
    collector_source = CollectSource.objects.get(code="sina_wow_pvp_2v2")
    cl = WowCollector(collector_source);
    videos = cl.collect()
