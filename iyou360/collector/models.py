# coding=utf8
from video.models import Category
from django.db import models
from provider.models import Provider 
import datetime

class CollectorDef(models.Model):
    name = models.CharField(max_length=20)
    class_type = models.CharField(max_length=100)
    provider = models.ForeignKey(Provider)
    
    def __unicode__(self):
        return self.class_type
    class Meta:
        app_label = 'collector'

class CollectSource(models.Model):
    STATUS_INIT = 0
    STATUS_FINISHED = 1
    STATUS_FAIL = 2
    STATUS_QUEUE = 3
    STATUS_RUNNING = 4
    
    STATUS_CHOICES = (
        (STATUS_INIT, "初始状态"),
        (STATUS_FINISHED, "完成"),
        (STATUS_FAIL, "失败"),
        (STATUS_QUEUE, "队列中"),
        (STATUS_RUNNING, "正在执行"),
    )
    
    code = models.CharField(max_length=100)
    provider = models.ForeignKey(Provider)
    url = models.CharField(max_length=200)
    url_params = models.CharField(max_length=300, null=True, blank=True)
    page_start = models.IntegerField()
    total_page = models.IntegerField()
    categorys = models.ManyToManyField(Category,null=True, blank=True)
    collector_def = models.ForeignKey(CollectorDef)
    status = models.IntegerField(default=0, choices=STATUS_CHOICES)
    create_time = models.DateTimeField(default=datetime.datetime.now())
    last_run_time = models.DateTimeField(null=True, blank=True)
    last_finish_time = models.DateTimeField(null=True, blank=True)
    download_out_img = models.BooleanField(default=False)
    
    def __unicode__(self):
        return self.code
    class Meta:
        app_label = 'collector'
        ordering = ["-id"]

