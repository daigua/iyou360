# coding=utf8
import urllib
import importlib
from models import CollectSource
from video.models import Video
import logging
import datetime,os

logger = logging.getLogger("collector")

video_img_save_path = os.path.dirname(__file__).replace('\\','/') + '/../staticfiles/static'

def download_video_preview_img(videos):
    for video in videos:
        img_url = video.preview_img_url
        sufix = img_url[img_url.rfind("."):]
        if(img_url.startswith(u"http://") and img_url.find(u"iyou360.com") == -1):
            import time
            time_dir = time.strftime(u"%Y/%m")
            dir_img = str(video_img_save_path) + "/" + time_dir
            if not os.path.exists(dir_img) :
                os.makedirs(dir_img)
            file_name = str(int(time.time() * 1000))+ sufix
            file_img = dir_img + "/" + file_name
            logger.info("download img, src : " + img_url + " to destnation : " + file_img)
            try:
                urllib.urlretrieve(img_url,file_img)
                video.preview_img_url = "/static/" + time_dir + "/" + file_name
            except Exception,e:
                logger.error(e)
                video.preview_img_url = "/static/images/zs.jpg"
            video.save()

class Collector(object):
    
    def __init__(self, collect_source):
        self.collect_source = collect_source
        self.url = collect_source.url.encode("ASCII")
        url_params = collect_source.url_params
        if(url_params and len(url_params) > 0) :
            for p in url_params.split(",") :
                p = p.split(":")
                self.url = self.url.replace("{"+p[0]+"}",urllib.quote(p[1].encode('utf8')))
                
    def collect(self):
        logger.info("begin to collect resource : %s",self.collect_source)
        self.collect_source.status = CollectSource.STATUS_RUNNING
        self.collect_source.last_run_time = datetime.datetime.now()
        self.collect_source.save()
        videos = []
        page = self.collect_source.page_start + self.collect_source.total_page - 1
        while page >= self.collect_source.page_start :
            try:
                local_url = self.url.replace("{page}",str(page))
                logger.info("collect URL: %s", local_url)
                subVs = self.collectVideos(local_url)
                if(subVs) :
                    videos += subVs
            except Exception,  e:
                logger.error(e)
            page-=1
            
        self.collect_source.status = CollectSource.STATUS_FINISHED
        self.collect_source.last_finish_time = datetime.datetime.now()
        self.collect_source.save()
        logger.info("collect finished ：%s ", self.collect_source)
        return videos
    
    def collectVideos(self, url):
        content = urllib.urlopen(url).read()
        return self.saveVideos(str(content))

    def saveVideos(self, content):
        videos = self.getVideos(content)
        videos.reverse()
        for video in videos :
            video.provider_code = self.collect_source.collector_def.provider.code
            self.completeVideo(video)
            try:
                checkV = Video.objects.get(title=video.title)
                logger.info("video has existed : %s ", video.title)
                continue
            except Video.DoesNotExist:
                logger.info("video is new : %s ,save it", video.title)
            if(self.collect_source.download_out_img):
                download_video_preview_img([video])
            video.save()
            logger.info("save video : %s ", video)
            if(hasattr(video,'lazy_categorys') and len(video.lazy_categorys) > 0):
                video.categorys = video.lazy_categorys
            else:
                video.categorys = self.collect_source.categorys.all()
        return videos

    @staticmethod
    def instance_by(collect_source):
        collector_def = collect_source.collector_def
        info = collector_def.class_type.split(".")
        md = importlib.import_module(info[0]+"."+info[1])
        collector = eval("md."+info[2]+"(collect_source)")
        return collector

