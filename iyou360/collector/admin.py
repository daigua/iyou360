#encoding=utf8
from django.contrib import admin
from models import CollectSource,CollectorDef
from collector import Collector
import thread

def CollectSource_status_init(modeladmin, request, queryset):
    queryset.update(status=CollectSource.STATUS_INIT)
    
CollectSource_status_init.short_description = "设为初始状态"

def CollectSource_status_finished(modeladmin, request, queryset):
    queryset.update(status=CollectSource.STATUS_FINISHED)
    
CollectSource_status_finished.short_description = "设为完成状态"

def CollectSource_status_fail(modeladmin, request, queryset):
    queryset.update(status=CollectSource.STATUS_FAIL)
    
CollectSource_status_fail.short_description = "设为失败状态"

def run_collect(source):
    collector = Collector.instance_by(source)
    collector.collect()
    
def CollectSource_run_collect(modeladmin, request, queryset):
    for source in queryset.all():
        thread.start_new_thread(run_collect, (source,))

CollectSource_run_collect.short_description = "立即执行"

class CollectSourceAdmin(admin.ModelAdmin):
    filter_horizontal = ("categorys",)
    list_display = ("code", "url","url_params","page_start","total_page","last_run_time", "last_finish_time", "create_time", "status")
    list_filter = ("status","collector_def",)
    actions = [CollectSource_run_collect, CollectSource_status_init, CollectSource_status_finished,CollectSource_status_fail]

class CollectorDefAdmin(admin.ModelAdmin):
    list_display = ("name","class_type")
    
admin.site.register(CollectSource, CollectSourceAdmin)
admin.site.register(CollectorDef, CollectorDefAdmin)


