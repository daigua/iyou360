import urllib
from pyquery import PyQuery
from video.models import Video
from collector import Collector
import random
from models import CollectSource

import logging

logger = logging.getLogger("collector")

#luren collector 
class LurenZSCollector(Collector):
    def getVideos(self, content):
        pq = PyQuery(content)
        v_items = pq.find(".content .sp_div dl")
        videos = []
        def callback(index, item):
            item = PyQuery(item)
            video = Video()
            py_title = item.find(".dl_dd_tit a")
            play_len = ""
            ref_url = py_title.attr("href")
            try:
                checkV = Video.objects.get(ref_url = ref_url)
                logger.info("ref_url : " + ref_url + " has existed, pass it") 
                return
            except Exception:
                pass
            title = py_title.text()
            p_img_url =item.find("dt img").attr("src")
            hits = random.randint(180, 354)
            video.title = title
            video.play_length = play_len
            video.ref_url = ref_url
            video.preview_img_url = p_img_url
            video.hits = hits
            videos.append(video)
        v_items.each(callback)
        return videos
    
    def completeVideo(self, video):
        content = urllib.urlopen(video.ref_url).read()
        pg = PyQuery(content)
        swf_url = pg.find(".content embed").eq(0).attr("src")
        video.swf_url = swf_url
        summary = pg.find(".content p").eq(1).html()
        video.summary = summary
        
if(__name__=="__main__"):
    collector_source = CollectSource.objects.get(code="luren_zs")
    youku = LurenZSCollector(collector_source);
    videos = youku.collect()
        