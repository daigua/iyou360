#encoding=utf8
from django import template
from sitepage.models import SitePage
register = template.Library()

@register.inclusion_tag("site_page_header.html")
def header(target="video_list", arg=None):
    pages = SitePage.objects.all()
    context = {'site_pages' : pages, 'target' : target}
    if(target == "video_list") :
        context["site_page"] = arg
    if(target == "video_show") :
        context['video'] = arg
    return context

@register.inclusion_tag("site_page_footer.html")
def footer():
    pass