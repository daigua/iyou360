from django.template import Library
register = Library()
        
@register.filter
def get_range(start, end = 0):
    return range(start, end)
