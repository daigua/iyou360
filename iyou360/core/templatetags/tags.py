#encoding=utf8
from django import template
from django.core.paginator import EmptyPage,PageNotAnInteger
from core.paginator import PrettyPaginator

class TagWithArgs(template.Node):
    
    def __init__(self, parser, token, ignore_failures=False):
        self.token = token
        self.parser = parser
        parser.compile_filter
        self.nodelist = self.get_nodelist()
        self.ignore_failures = ignore_failures
        
    def get_nodelist(self):
        pass
    
    def render(self, context):
        args = self.get_tag_args(context)
        context.push()
        self.set_context(context, args)
        output = self.nodelist.render(context)
        context.pop()
        return output
    
    def set_context(self, context, tag_args):
        pass
    
    def get_tag_args(self, context):
        token = self.token
        parser = self.parser
        seg = token.split_contents()
        if(len(seg) == 1):
            return {}
        args = {}
        args_seg = seg[1:]
        for arg_str in args_seg:
            arg_seg = arg_str.split("=")
            args[arg_seg[0]] = parser.compile_filter(arg_seg[1]).resolve(context, self.ignore_failures)
        return args
        
class PaginationTag(TagWithArgs):
    
    def set_context(self, context, tag_args):
        limit_args = {'page':1, 'page_size':20} 
        if(tag_args.has_key("page")):
            limit_args["page"] = tag_args["page"]
        if(tag_args.has_key("page_size")):
            limit_args["page_size"] = tag_args["page_size"]
        lists = self.get_list(tag_args)
        page_show_count = 10
        if(tag_args.has_key("page_show_count")):
            page_show_count = tag_args["page_show_count"]
        paginator = PrettyPaginator(lists, limit_args["page_size"], page_show_count=page_show_count)
        try:
            lists = paginator.page(limit_args["page"])
        except PageNotAnInteger:
            lists = paginator.page(1)
        except EmptyPage:
            lists = paginator.page(paginator.num_pages)
        context[self.get_list_context_key()] = lists
    
    def clean_pagination_args(self, tag_args):
        if(tag_args.has_key("page")):
            del tag_args["page"]
        if(tag_args.has_key("page_size")):
            del tag_args["page_size"]
        if(tag_args.has_key("page_show_count")):
            del tag_args["page_show_count"]
        