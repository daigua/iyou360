from django.core.paginator import Paginator

class PrettyPaginator(Paginator):
    
    def __init__(self, object_list, per_page, orphans=0, allow_empty_first_page=True, page_show_count=10):
        Paginator.__init__(self, object_list, per_page, orphans, allow_empty_first_page)
        self.page_show_count = page_show_count
        self.start_page, self.end_page = None,None
    
    def page(self, number):
        page = Paginator.page(self, number)
        self.handle_page_split(page)
        return page
    
    def start_page_num(self):
        return self.start_page
    def end_page_num(self):
        return self.end_page
    def end_page_num_range(self):
        return self.end_page + 1
    
    def handle_page_split(self,page):
        current_page = page.number
        start_page = current_page - self.page_show_count / 2
        if(start_page < 1):
            start_page = 1
        self.start_page = start_page
        total_page = page.paginator.num_pages
        end_page = start_page + self.page_show_count - 1
        if(end_page < 1):
            end_page = 1
        if(end_page > total_page):
            end_page = total_page
        if(end_page == total_page):
            self.start_page = total_page - self.page_show_count + 1
        if(self.start_page < 1):
            self.start_page = 1
        self.end_page = end_page
        
