from django.contrib import admin
from models import Provider, AppAuth

admin.site.register(Provider)
admin.site.register(AppAuth)