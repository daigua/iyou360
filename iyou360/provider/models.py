from django.db import models

class Provider(models.Model):
    name = models.CharField(max_length=20)
    code = models.CharField(max_length=20)
    
    def __unicode__(self):
        return self.name
    
class AppAuth(models.Model):
    provider_name = models.CharField(max_length=20)
    key = models.CharField(max_length=50)
    secret = models.CharField(max_length=50)
    provider = models.ForeignKey(Provider)
    
    def __unicode__(self):
        return self.provider_name