#encoding=utf8

def page_limit(page, page_size):
    if(page < 1):
        page = 1
    offset = (page - 1) * page_size
    return {"offset" : offset, "page_size" : page_size}