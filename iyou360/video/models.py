# coding=utf8
from django.db import models
import datetime
import random
import string

class Category(models.Model):
    name = models.CharField(max_length=50)
    parent = models.ForeignKey('self',null=True,blank=True,related_name='children')
    index = models.IntegerField()
    
    def all_children(self):
        all_children = []
        categorys = [category for category in self.children.all()]
        while len(categorys) > 0 :
            children = categorys.pop()
            all_children.append(children)
            categorys += children.children.all()
        return all_children
    
    def all_parent(self):
        parents = []
        parent = self.parent.all()
        while parent :
            parents.append(parent)
            parent = parent.parent.all()
        return parents.reverse()
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ['index']
    
source = "ABCDEFGHIGKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
def random_video_key():
    return string.join(random.sample(source,20),"")
    
class Video(models.Model):
    
    STATUS_INIT = 0
    STATUS_NORMAL = 1
    STATUS_HIDDEN = 2
    
    STAUTS_CHOICES = (
        (STATUS_INIT, '初始'),
        (STATUS_NORMAL, '正常'),
        (STATUS_HIDDEN, '屏蔽'),
    )
    title = models.CharField(max_length=100)
    play_length = models.CharField(max_length=10)
    summary = models.TextField(blank=True, null=True)
    tag = models.CharField(max_length=200,blank=True, null=True)
    categorys = models.ManyToManyField(Category)
    provider_code = models.CharField(max_length=20,blank=True, null=True)
    create_time = models.DateTimeField(default=datetime.datetime.now())
    swf_url = models.CharField(max_length=500)
    hits = models.IntegerField(default=0)
    preview_img = models.ImageField(upload_to='iyou360/static/preview/%Y%m%d/',blank=True, null=True)
    preview_img_url = models.CharField(max_length=200,blank=True, null=True)
    ref_url = models.CharField(max_length=200, blank=True, null=True)
    status = models.IntegerField(choices=STAUTS_CHOICES, default=STATUS_INIT)
    video_key = models.CharField(max_length=30, default=random_video_key)

    def __unicode__(self):
        return self.title
    class Meta:
        ordering = ['-id']
     

    

    