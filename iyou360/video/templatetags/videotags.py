#encoding=utf8
from django import template
from video.models import Video,Category
from core.templatetags.tags import PaginationTag,TagWithArgs
register = template.Library()

@register.tag(name="videos")
def videos(parser, token):
    return VideoNode(parser, token)

class VideoNode(PaginationTag):
    
    def get_nodelist(self):
        nodelist = self.parser.parse(('endvideos',))
        self.parser.delete_first_token()
        return nodelist
    
    def get_list_context_key(self):
        return "videos"   
    
    def get_list(self, tag_args):
        order_by=None
        if(tag_args.has_key("order_by")):
            order_by = tag_args["order_by"]
            del tag_args["order_by"]
        category_recursion = 0
        if(tag_args.has_key("category_recursion")):
            category_recursion = tag_args['category_recursion']
            del tag_args['category_recursion']
        if(tag_args.has_key("category_id")):
            category_id = tag_args['category_id']
            del tag_args['category_id']
            if category_id != "" :
                if(category_recursion):
                    category = Category.objects.get(id = category_id)
                    categorys = category.all_children()
                    tag_args['categorys__in'] = categorys + [category]
                else:
                    tag_args['categorys__id__in'] = [category_id]
        self.clean_pagination_args(tag_args)
        
        if(order_by):
            videos = Video.objects.filter(**tag_args).order_by(order_by)
        else:
            videos = Video.objects.filter(**tag_args)
        
        return videos

@register.tag(name="videocategorys")
def videocategorys(parser, token):
    return VideoCategoryNode(parser, token)

class VideoCategoryNode(TagWithArgs):
    def get_nodelist(self):
        nodelist = self.parser.parse(('endvideocategorys',))
        self.parser.delete_first_token()
        return nodelist
    
    def set_context(self, context, tag_args):
        parent = 0
        if(tag_args.has_key('parent')):
            parent = int(tag_args['parent'])
        categorys = Category.objects.filter(parent__id=parent)
        context['videocategorys'] = categorys