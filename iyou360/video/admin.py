# encoding=utf8
from django.contrib import admin
from models import Category, Video
import logging,thread
from collector.collector import download_video_preview_img

logger = logging.getLogger("default")

def video_status_init(modeladmin, request, queryset):
    queryset.update(status=0)
video_status_init.short_description = u"设为初始状态"

def video_status_normal(modeladmin, request, queryset):
    queryset.update(status=1)
video_status_normal.short_description = u"设为正常状态"

def video_status_hidden(modeladmin, request, queryset):
    queryset.update(status=2)

video_status_hidden.short_description = u"设为屏蔽状态"


def download_out_img(modeladmin, request, queryset):
    thread.start_new_thread(download_video_preview_img, (queryset.all(),))

download_out_img.short_description = "下载外部视频图片"

class VideoAdmin(admin.ModelAdmin):
    filter_horizontal = ("categorys",)
    list_display = ("title","id","video_key","provider_code","play_length","ref_url","hits","create_time","status")
    list_filter = ("categorys","status","provider_code",)
    search_fields = ("title",)
    ordering = ('-id',)
    actions = [video_status_init,video_status_normal,video_status_hidden,download_out_img]
    
admin.site.register(Category)
admin.site.register(Video, VideoAdmin)
