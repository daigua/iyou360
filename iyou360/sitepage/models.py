# encoding=utf8
from django.db import models
from video.models import Category

class SitePage(models.Model):
    
    STATUS_NORMAL = 1
    STATUS_HIDDEN = 0
    STATUS_CHOICES = (
        (STATUS_NORMAL, "正常"),
        (STATUS_HIDDEN, "隐藏")
    )
    
    name = models.CharField(max_length=30)
    #绑定的视频分类
    video_category = models.ForeignKey(Category, blank=True, null=True)
    #本页面的link 仅仅对外链有效
    link = models.CharField(max_length=200)
    
    index = models.IntegerField()
    #是否在新窗口中打开，默认在当前窗口打开
    open_in_blank = models.BooleanField(default=False)
    
    status = models.IntegerField(default=STATUS_HIDDEN, choices=STATUS_CHOICES)
    
    def is_current(self, link):
        if(link == self.link):
            return True
        return False
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ['index']
        
