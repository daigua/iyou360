from django.conf.urls import patterns

urlpatterns = patterns('sitepage.views',
    (r'^$', 'home'),
    (r'^v/(?P<link>[^_]+)(?:|(?:_(?P<argstr>[&\w\,\:\-_]+)))\.html$', 'site_page'),
    (r'^v_show/(?P<vid>\d+)_(?P<key>\w+)\.html$', 'video_show'),
)

