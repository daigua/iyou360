from django.contrib import admin
from models import SitePage

class SitePageAdmin(admin.ModelAdmin):
    list_display = ("name", "link","open_in_blank","index","status")

admin.site.register(SitePage,SitePageAdmin)
