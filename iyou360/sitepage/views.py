# encoding=utf8
from sitepage.models import SitePage
from video.models import Video

from django.shortcuts import render_to_response, RequestContext

def home(request):
    return render_to_response("video_list.html", context_instance=RequestContext(request))

def site_page(request, link, argstr):
    site_page = SitePage.objects.get(link=link)
    context = {'site_page':site_page}
    if(argstr) :
        arg_seg = argstr.split("&")
        for str_arg in  arg_seg:
            arg = str_arg.split(":")
            context[arg[0]] = arg[1]
    if not context.has_key("c"):
        context["c"] = site_page.video_category.id
    else:
        context["c"] = int(context["c"])
    return render_to_response("video_list.html",context, context_instance=RequestContext(request))

def video_show(request, vid, key):
    video = Video.objects.get(id=vid,video_key=key)
    video.hits += 1
    video.save()
    return render_to_response("video_show.html",{'video':video})